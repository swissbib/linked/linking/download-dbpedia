import logging
import os
import shutil
import sys

import requests
import yaml
from SPARQLWrapper import SPARQLWrapper, JSON
from yaml import BaseLoader

if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                        format="[%(levelname)s] [%(module)s] %(message)s")

    if os.path.exists('/configs/app.yml'):
        yml_file = open('/configs/app.yml')
    else:
        yml_file = open('app.yml')

    conf = yaml.load(yml_file, Loader=BaseLoader)
    yml_file.close()

    sparql = SPARQLWrapper(conf['sparql']['endpoint'])

    for language in ['de', 'en', 'fr', 'it', 'commons']:
        with open(conf['sparql']['files'][language], 'r') as fp:
            query = fp.read()

        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        for binding in results['results']['bindings']:
            download_link: str = binding['file']['value']
            logging.info(f"Check file from download link {download_link} for download.")
            file_name_artifact: str = download_link.split('/')[-1]
            latest_version: str = binding['latestVersion']['value'].replace('.', '-')
            file_name = f'{latest_version}-{file_name_artifact}'

            if os.path.exists(f"{conf['output'][language]}/{file_name}"):
                logging.info(f"Already downloaded {file_name}. Skipping!")
                continue
            else:
                # delete old files of the same artifact.
                for root, directories, files in os.walk(conf['output'][language]):
                    for file in files:
                        if file_name_artifact in file:
                            os.remove(os.path.join(root, file))
                            logging.info(f"Deleting old file {file}")

            logging.info(f"Download file from {download_link}.")
            with requests.get(download_link, stream=True) as response:
                if response.ok:
                    with open(f'/tmp/{file_name}', 'wb') as f:
                        for chunk in response.iter_content(chunk_size=8192):
                            if chunk:  # filter out keep-alive new chunks
                                f.write(chunk)
                    shutil.move(f'/tmp/{file_name}', f'{conf["output"][language]}/{file_name}')

            logging.info(f"Finished download from {download_link} into {conf['output'][language]}/{file_name}")

    logging.info("Finished download for dbpedia.")

