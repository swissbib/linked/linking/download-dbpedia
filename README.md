# Download DBpedia Dump

Once started the downloader uses SPARQL requests on the 
[Databus SPARQL Endpoint](https://databus.dbpedia.org/repo/sparql) to get 
the download links for all the files.

Currently loads all the generic releases and not a improved dataset. These
will be available at a later date. Downloads all the files
for the languages de, en, fr, it & commons.
